import os
from optparse import OptionParser
import subprocess
import json

_html_header = """
<!doctype html>
<html ng-app>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
body {
font-family: sans-serif;
}
input[type=search] {

padding: 12px 20px;
margin: 8px 0;
display: inline-block;
border: 1px solid #ccc;
border-radius: 4px;
box-sizing: border-box;
width : 66%;
font-size:16px;

-webkit-appearance: none;
}
table{
table-layout: fixed;
white-space: nowrap;

}

.local_path
{
    
}

.type
{
    
}

.remote{
    font-size: 12px
}

</style>

</head>
<body>
<center>
<input type="search" id="searchInput" name="search_element">
<table id="fbody">
"""


_html_footer = """
</table>
</center>
</body>
<script type="text/javascript">
$("#searchInput").keyup(function() {
var rows = $("#fbody").find("tr").hide();
var data = this.value.split(" ");
$.each(data, function(i, v) {
rows.filter(":contains('" + v + "')").show();
});
});
</script>
</html>
"""


def check_output(*popenargs, **kwargs):
    r"""Run command with arguments and return its output as a byte string.
    Backported from Python 2.7 as it's implemented as pure python on stdlib.
    >>> check_output(['/usr/bin/python', '--version'])
    Python 2.6.2
    """
    process = subprocess.Popen(stderr=subprocess.PIPE, stdout=subprocess.PIPE, *popenargs, **kwargs)
    output, unused_echangelog = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        error = subprocess.CalledProcessError(retcode, cmd)
        error.output = output
        raise error
    return output

def repo_type(path):
    git = is_git(path)
    if git:
        return git
    hg = is_hg(path)
    if hg:
        return hg

def _run_cmd(cmd, repo):
    # return subprocess.check_output(cmd,shell=True, cwd=repo) # Python >= 2.7
    cwd = os.getcwd()
    os.chdir(repo)
    result=''
    try:
        result = check_output(cmd.split(' '))
    except:
        pass
    finally:
        os.chdir(cwd)
    return result.split('\n')

def get_remote_url(repo_type, repo):
    if repo_type == 'git':
        cmd = 'git config --get remote.origin.url'
    if repo_type == 'hg':
        cmd = 'hg paths default --pager false'

    return _run_cmd(cmd, repo)[0] 



def is_git(path):
    if os.path.isdir(os.path.join(path, '.git')):
        return 'git'



def is_hg(path):
    if os.path.isdir(os.path.join(path, '.hg')):
        return 'hg'


def is_repo(folder, path, report, jout):
    rtype = repo_type(path)
    if rtype:
        if jout:
            item = dict()
            item["type"] = rtype
            item["local_path"] = path
            item["remote"] = get_remote_url(rtype, path)
            report.append(item) 
        else:
            print( "{0},{1},{2}".format(rtype,path, get_remote_url(rtype, path)) )
        return True
    return False

def html(report):
    print ("{0}".format(_html_header))
    for item in report:
        print("<tr>")
        print("<td class=\"type\">[{0}] </td>".format(item["type"]))
        print("<td class=\"local_path\">{0}</td>".format(item["local_path"]))
        print("<td></td>")
        print("<td class=\"remote\">{0}</td>".format(item["remote"]))
        print("</tr>")
    print ("{0}".format(_html_footer))


def run(folder,jout, web):
    report = list()

    for root, dirs, files in os.walk(folder):
        for dir_ in dirs:
            if is_repo(folder, root, report, jout or web):
                break
    if jout:
        print(json.dumps(report, ensure_ascii=False, indent=4))
    if web:
        html(report)

            


def main():

    parser = OptionParser()
    parser.add_option(
        "-i",
        "--input",
        default=None,
        dest="folder",
        help="Folder to scan recursively.",
        metavar="FOLDER")
    parser.add_option("-j", "--json", default=False, dest="jout",
        action='store_true', help="Generates json (not compativle with -w)")
    parser.add_option("-w", "--web", default=False, dest="web",
        action='store_true', help="Generates html (not compativle with -j)")

    (options, args) = parser.parse_args()
    if options.folder is None:
        options.folder = os.getcwd()
    if options.jout and options.web:
        parser.error('')

    run(options.folder,options.jout, options.web)


if __name__ == '__main__':
    main()
